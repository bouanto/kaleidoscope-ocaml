let rec lex stream =
    match Stream.peek stream with
    | Some (' ' | '\n' | '\r' | '\t') ->
        Stream.junk stream;
        Stream.slazy (fun () -> lex stream)
    | Some ('A' .. 'Z' | 'a' .. 'z' as c) ->
        let buffer = Buffer.create 1 in
        Buffer.add_char buffer c;
        Stream.junk stream;
        lex_ident buffer stream
    | Some('0' .. '9' as c) ->
        let buffer = Buffer.create 1 in
        Buffer.add_char buffer c;
        Stream.junk stream;
        lex_number buffer stream
    | Some('#') ->
        lex_comment stream
    | Some(c) ->
        Stream.junk stream;
        Stream.iapp (Stream.ising (Token.Kwd c)) (Stream.slazy (fun () -> lex stream))
    | None ->
        Stream.sempty

and lex_ident buffer stream =
    match Stream.peek stream with
    | Some ('A' .. 'Z' | 'a' .. 'z' | '0' .. '9' as c) ->
        Stream.junk stream;
        Buffer.add_char buffer c;
        lex_ident buffer stream
    | _ ->
        let ident_stream =
            match Buffer.contents buffer with
            | "binary" -> Stream.ising Token.Binary
            | "def" -> Stream.ising Token.Def
            | "else" -> Stream.ising Token.Else
            | "extern" -> Stream.ising Token.Extern
            | "for" -> Stream.ising Token.For
            | "if" -> Stream.ising Token.If
            | "in" -> Stream.ising Token.In
            | "then" -> Stream.ising Token.Then
            | "unary" -> Stream.ising Token.Unary
            | "var" -> Stream.ising Token.Var
            | id -> Stream.ising (Token.Ident id)
        in
        Stream.iapp ident_stream (Stream.slazy (fun () -> lex stream))

and lex_number buffer stream =
    match Stream.peek stream with
    (* TODO: handle errors for input like 9.12.23.34 *)
    | Some ('0' .. '9' | '.' as c) ->
        Buffer.add_char buffer c;
        Stream.junk stream;
        lex_number buffer stream
    | _ ->
        let num_stream = Stream.ising (Token.Number (float_of_string (Buffer.contents buffer))) in
        Stream.iapp num_stream (Stream.slazy (fun () -> lex stream))

and lex_comment stream =
    match Stream.peek stream with
    | Some('\n') ->
        Stream.junk stream;
        Stream.slazy (fun () -> lex stream)
    | Some(_) ->
        Stream.junk stream;
        lex_comment stream
    | None ->
        Stream.sempty
