type expr =
    | Number of float
    | Variable of string
    | Binary of char * expr * expr
    | Call of string * expr array
    | If of expr * expr * expr
    | For of string * expr * expr * expr option * expr
    | Unary of char * expr
    | Var of (string * expr option) array * expr

type proto =
    | Prototype of string * string array
    | BinOpPrototype of string * string array * int

type func =
    Function of proto * expr
