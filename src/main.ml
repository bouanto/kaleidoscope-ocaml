open Ctypes
open Lexer
open Llvm
open Llvm_scalar_opts
open Llvm_target

external putchard: float -> unit = "putchard"
external printd: float -> unit = "putchard"

let rec main_loop pass_manager execution_engine stream =
    match Stream.peek stream with
    | None -> ()
    | Some (Token.Kwd ';') ->
        Stream.junk stream;
        main_loop pass_manager execution_engine stream
    | Some token -> (
        try match token with
        | Token.Def ->
            let func = Parser.parse_definition stream in
            print_endline "parsed a function definition";
            dump_value (Gen.codegen_func pass_manager func)
        | Token.Extern ->
            let extern = Parser.parse_extern stream in
            print_endline "parsed an extern";
            dump_value (Gen.codegen_proto extern)
        | _ ->
            let expr = Parser.parse_toplevel stream in
            print_endline "parsed a top-level expr";
            let func = Gen.codegen_func pass_manager expr in
            dump_value func;
            Llvm_executionengine.add_module Gen.modul execution_engine;
            let func_type = Foreign.funptr (void @-> returning double) in
            let func = Llvm_executionengine.get_function_address (State.current_anon_name ()) func_type execution_engine in
            print_string "Evaluated to ";
            print_float (func ());
            print_newline ();
            incr State.expr_count;
            Llvm_executionengine.remove_module Gen.modul execution_engine;
        with Stream.Error error ->
            Stream.junk stream;
            print_endline error
    );
    print_string "ready> ";
    flush stdout;
    main_loop pass_manager execution_engine stream

let main () =
    Hashtbl.add Parser.binop_precedence '=' 2;
    Hashtbl.add Parser.binop_precedence '<' 10;
    Hashtbl.add Parser.binop_precedence '+' 20;
    Hashtbl.add Parser.binop_precedence '-' 20;
    Hashtbl.add Parser.binop_precedence '*' 40;
    print_string "ready> ";
    flush stdout;
    let stream = Lexer.lex (Stream.of_channel stdin) in

    let target_triple = Target.default_triple () in
    Llvm_all_backends.initialize ();
    let target = Target.by_triple target_triple in
    let cpu = "generic" in
    let features = "" in
    let opt = CodeGenOptLevel.Default in
    let reloc_mode = RelocMode.Default in
    let code_model = CodeModel.Default in
    let target_machine =
        TargetMachine.create ~triple:target_triple ~cpu ~features ~level:opt ~reloc_mode ~code_model target
    in
    set_data_layout (DataLayout.as_string (TargetMachine.data_layout target_machine)) Gen.modul;
    set_target_triple target_triple Gen.modul;

    ignore (Llvm_executionengine.initialize ());
    let execution_engine = Llvm_executionengine.create Gen.modul in
    let pass_manager = PassManager.create_function Gen.modul in
    add_instruction_combination pass_manager;
    add_reassociation pass_manager;
    add_gvn pass_manager;
    add_cfg_simplification pass_manager;
    add_memory_to_register_promotion pass_manager;
    ignore (PassManager.initialize pass_manager);

    main_loop pass_manager execution_engine stream;
    dump_module Gen.modul;

    let filename = "output/output.o" in
    TargetMachine.emit_to_file Gen.modul CodeGenFileType.ObjectFile filename target_machine;
    print_endline ("Wrote " ^ filename ^ ".");
;;

main ()
