let expr_count = ref 0

let current_anon_name () =
    "__anon_expr" ^ (string_of_int !expr_count)
