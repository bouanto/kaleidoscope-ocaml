let binop_precedence: (char, int) Hashtbl.t =
    Hashtbl.create 10

let precedence c =
    try Hashtbl.find binop_precedence c
    with Not_found -> -1

let eat stream token expected =
    if Stream.next stream = token then
        ()
    else
        raise (Stream.Error ("parse error: " ^ expected))

let parse_ident stream expected =
    match Stream.peek stream with
    | Some (Token.Ident id) ->
        Stream.junk stream;
        id
    | _ ->
        raise (Stream.Error ("expected identifier after " ^ expected))

let rec parse_primary stream =
    match Stream.peek stream with
    | Some (Token.Number n) ->
        Stream.junk stream;
        Ast.Number n
    | Some (Token.Kwd '(') ->
        Stream.junk stream;
        let e = parse_expr stream in
        eat stream (Token.Kwd ')') "expected ')'";
        e
    | Some (Token.Ident id) ->
        Stream.junk stream;
        let rec parse_args accumulator stream =
            match parse_expr stream with
            | expr -> (
                match Stream.peek stream with
                | Some (Token.Kwd ',') ->
                    Stream.junk stream;
                    let arg = parse_args (expr :: accumulator) stream in
                    arg
                | _ -> expr :: accumulator
            )
            | exception Stream.Error _ -> accumulator (* FIXME: Not sure about this. *)
        in
        let rec parse_ident id stream =
            match Stream.peek stream with
            | Some (Token.Kwd '(') ->
                Stream.junk stream;
                let args = parse_args [] stream in
                eat stream (Token.Kwd ')') "expected ')'";
                Ast.Call (id, Array.of_list (List.rev args))
            | _ ->
                Ast.Variable id
        in
        parse_ident id stream
    | Some Token.If ->
        Stream.junk stream;
        let condition = parse_expr stream in
        eat stream Token.Then "expected 'then'";
        let true_expr = parse_expr stream in
        eat stream Token.Else "expected 'else'";
        let false_expr = parse_expr stream in
        Ast.If (condition, true_expr, false_expr)
    | Some Token.For ->
        Stream.junk stream;
        let var_name = parse_ident stream "for" in
        eat stream (Token.Kwd '=') "expected '=' after for";
        let start = parse_expr stream in
        eat stream (Token.Kwd ',') "expected ',' after for";
        let end_val = parse_expr stream in
        let step =
            (match Stream.peek stream with
            | Some (Token.Kwd ',') ->
                Stream.junk stream;
                let step = parse_expr stream in
                Some step
            | _ -> None
            )
        in
        eat stream Token.In "expected 'in' after for";
        let body = parse_expr stream in
        Ast.For (var_name, start, end_val, step, body);
    | Some Token.Var ->
        Stream.junk stream;
        let id = parse_ident stream "var" in
        let init = parse_var_init stream in
        let var_names = parse_var_names [(id, init)] stream in
        eat stream Token.In "expected 'in' keyword after 'var'";
        let body = parse_expr stream in
        Ast.Var (Array.of_list (List.rev var_names), body)
    | _ -> raise (Stream.Error "unknown token when expecting an expression")

and parse_var_init stream =
    match Stream.peek stream with
    | Some (Token.Kwd '=') ->
        Stream.junk stream;
        let expr = parse_expr stream in
        Some expr
    | _ -> None

and parse_var_names accumulator stream =
    match Stream.peek stream with
    | Some (Token.Kwd ',') ->
        Stream.junk stream;
        let id = parse_ident stream "var" in
        let init = parse_var_init stream in
        parse_var_names ((id, init) :: accumulator) stream
    | _ -> accumulator

and parse_expr stream =
    let left = parse_unary stream in
    parse_bin_rhs 0 left stream

and parse_bin_rhs expr_precedence left stream =
    match Stream.peek stream with
    | Some (Token.Kwd c) when Hashtbl.mem binop_precedence c ->
        let token_precedence = precedence c in
        if token_precedence < expr_precedence then
            left
        else (
            Stream.junk stream;
            let right = parse_unary stream in
            let right =
                match Stream.peek stream with
                | Some (Token.Kwd c2) ->
                    let next_precedence = precedence c2 in
                    if token_precedence < next_precedence then
                        parse_bin_rhs (token_precedence + 1) right stream
                    else
                        right
                | _ -> right
            in
            let left = Ast.Binary (c, left, right) in
            parse_bin_rhs expr_precedence left stream
        )
    | _ -> left

and parse_unary stream =
    match Stream.peek stream with
    | Some (Token.Kwd op) when op != '(' && op != ')' ->
        Stream.junk stream;
        let operand = parse_expr stream in
        Ast.Unary (op, operand)
    | _ -> parse_primary stream

let parse_prototype stream =
    let rec parse_args accumulator stream =
        match Stream.peek stream with
        | Some (Token.Ident id) ->
            Stream.junk stream;
            parse_args (id :: accumulator) stream
        | _ -> accumulator
    in
    let parse_operator stream =
        match Stream.peek stream with
        | Some Token.Binary ->
            Stream.junk stream;
            "binary", 2
        | Some Token.Unary ->
            Stream.junk stream;
            "unary", 1
        | _ -> raise (Stream.Error "expected function name in prototype")
    in
    let parse_binary_precedence stream =
        match Stream.peek stream with
        | Some (Token.Number num) ->
            Stream.junk stream;
            int_of_float num
        | _ -> 30
    in
    match Stream.peek stream with
    | Some (Token.Ident id) ->
        Stream.junk stream;
        eat stream (Token.Kwd '(') "expected '(' in prototype";
        let args = parse_args [] stream in
        eat stream (Token.Kwd ')') "expected ')' in prototype";
        Ast.Prototype (id, Array.of_list (List.rev args))
    | Some _ ->
        let (prefix, kind) = parse_operator stream in
        let op =
            match Stream.peek stream with
            | Some (Token.Kwd op) ->
                Stream.junk stream;
                op
            | _ -> raise (Stream.Error "expected an operator")
        in
        let binary_precedence = parse_binary_precedence stream in
        eat stream (Token.Kwd '(') "expected '(' in prototype";
        let args = parse_args [] stream in
        eat stream (Token.Kwd ')') "expected ')' in prototype";
        let name = prefix ^ (String.make 1 op) in
        let args = Array.of_list (List.rev args) in
        if Array.length args != kind then
            raise (Stream.Error "invalid number of operators for operator")
        else
            if kind = 1 then
                Ast.Prototype (name, args)
            else
                Ast.BinOpPrototype (name, args, binary_precedence)
    | _ ->
        raise (Stream.Error "expected function name in prototype")

let parse_definition stream =
    match Stream.peek stream with
    | Some Token.Def ->
        Stream.junk stream;
        let prototype = parse_prototype stream in
        let expr = parse_expr stream in
        Ast.Function (prototype, expr)
    | _ ->
        raise (Stream.Error "expected def in function definition")

let parse_extern stream =
    match Stream.peek stream with
    | Some Token.Extern ->
        Stream.junk stream;
        parse_prototype stream
    | _ ->
        raise (Stream.Error "expected extern in extern")

let parse_toplevel stream =
    let expr = parse_expr stream in
    print_endline ("Generating " ^ (State.current_anon_name ()));
    Ast.Function (Ast.Prototype (State.current_anon_name (), [||]), expr)
