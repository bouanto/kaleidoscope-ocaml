type token =
    | Binary
    | Def
    | Else
    | Extern
    | For
    | Ident of string
    | If
    | In
    | Number of float
    | Kwd of char
    | Then
    | Unary
    | Var

let string_of_token token =
    match token with
    | Binary -> "binary"
    | Def -> "def"
    | Else -> "else"
    | Extern -> "extern"
    | For -> "for"
    | Ident _ -> "ident"
    | If -> "if"
    | In -> "in"
    | Number _ -> "number"
    | Kwd _ -> "kwd"
    | Then -> "in"
    | Unary -> "unary"
    | Var -> "var"
